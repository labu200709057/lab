package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Object> shapes = new ArrayList<Object>();

	
	public double calculateTotalArea(){
		double totalArea = 0;

		for (Object shape : shapes){
			if(shape instanceof Circle){
				Circle circle = (Circle) shape;
				totalArea += circle.area();
			}
			if(shape instanceof Rectangle){
				Rectangle rectangle = (Rectangle) shape;
				totalArea += rectangle.area();
			}
			if(shape instanceof Square){
				Square square = (Square) shape;
				totalArea += square.area();
			}


		}

		return totalArea;
	}
	
	public void addShape(Object objects) {

		shapes.add(objects);
	}
	

}
