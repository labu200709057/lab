package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();

            int row = 0, col = 0;

            try {
                System.out.print("Player " + player + " enter row number:");
                row = Integer.valueOf(reader.nextLine());
                if (row <= 0 || row > 3) {
                    throw new InvalidMoveException();
                }
            }catch (Exception e) {
                System.out.println("Invalid row number!");
                continue;
            }

            while (true) {
                try {
                    System.out.print("Player " + player + " enter column number:");
                    col = Integer.valueOf(reader.nextLine());
                    if (col <= 0 || col > 3) {
                        throw new InvalidMoveException();
                    }
                    board.checkBoard(row, col);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid column number!");
                    continue;
                } catch (InvalidMoveException e) {
                    System.out.println("Invalid move");
                    continue;
                }
                break;
            }


            board.move(row, col);
            System.out.println(board);
        }


        reader.close();
    }


}
