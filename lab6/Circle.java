public class Circle {
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }
    public double area(){
        return Math.PI * radius * radius;
    }
    public double perimeter(){
        return 2 * Math.PI *radius;
    }
    public boolean intersect(Circle circle2){
        return distance(circle2) < circle2.radius + radius;
    }

    public double distance(Circle circle2){
        int circle2x= circle2.center.xCoord;
        int circle2y = circle2.center.yCoord;
        double distance = Math.pow(center.xCoord - circle2x,2) + Math.pow(center.yCoord - circle2y,2);
        return Math.sqrt(distance);
    }
}
