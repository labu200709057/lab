public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft;
    Point topRight;
    Point bottomLeft;
    Point bottomRight;

    public Rectangle(int sideA,int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
        topRight = new Point(topLeft.xCoord, topLeft.yCoord + sideB);
        bottomLeft = new Point(topLeft.xCoord + sideA, topLeft.yCoord);
        bottomRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord + sideB);
    }

    public int area(){
        return sideA * sideB;
    }
    public int perimeter(){
        return (sideA*2)+(sideB*2);
    }
    public Point[] corners(){
        Point[] points = new Point[4];
        points[0] = topLeft;
        points[1] = topRight;
        points[2] = bottomLeft;
        points[3] = bottomRight;
        return points;
    }
}
