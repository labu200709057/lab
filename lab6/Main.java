public class Main {
    public static void main(String[] args){
        Rectangle rectangle = new Rectangle(3,5,new Point(0,0));
        System.out.println(rectangle.area());
        System.out.println(rectangle.perimeter());

        for (Point point : rectangle.corners()){
            System.out.println(point);
        }
        Circle circle = new Circle(10,new Point(7,8));
        System.out.println(circle.area());
        System.out.println(circle.perimeter());
        Circle circle2 = new Circle(6,new Point(3,8));
        System.out.println(circle.intersect(circle2));
    }
}
