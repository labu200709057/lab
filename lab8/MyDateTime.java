import java.text.StringCharacterIterator;
import java.util.Objects;

public class MyDateTime extends Object{
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    @Override
    public String toString() {
        return date + " " + time;
    }
    public void incrementDay(){
        date.incrementDay();
    }
    public void incrementHour(){
        incrementHour(1);
    }
    public void incrementHour(int diff){
        int dayDiff = time.incrementHour(diff);
        if(dayDiff<0){
            date.decrementDay(-dayDiff);
        }
        else
        date.incrementDay(dayDiff);
    }
    public void decrementHour(int diff){
        incrementHour(-diff);
    }
    public void incrementMinute(int diff){
        int dayDiff = time.incrementMinute(diff);
        if(dayDiff<0) {
            date.decrementDay(-dayDiff);
        }
        else
            date.incrementDay(dayDiff);
    }
    public void decrementMinute(int diff){
        incrementMinute(-30);
    }
    public void incrementYear(int diff){
        date.incrementYear(diff);
    }
    public void decrementDay(){
        date.decrementDay();
    }
    public void decrementYear(){
        date.decrementYear();
    }
    public void decrementMonth(){
        date.decrementMonth();
    }
    public void incrementDay(int i){
        date.incrementDay(i);
    }
    public void decrementMonth(int i){
        date.decrementMonth(i);
    }
    public void decrementDay(int i){
        date.decrementDay(i);
    }
    public void incrementMonth(int i){
        date.incrementMonth(i);
    }
    public void decrementYear(int i){
        date.decrementYear(i);
    }
    public void incrementMonth(){
        date.incrementMonth();
    }
    public void incrementYear(){
        date.incrementYear();
    }
    public boolean isBefore(MyDateTime anotherDateTime){
        int olddate = date.year+ date.month+date.day+ time.hour+time.minute;
        int newdate = anotherDateTime.date.year+anotherDateTime.date.month+anotherDateTime.date.day+anotherDateTime.time.hour+anotherDateTime.time.minute;

        return olddate < newdate;
    }


    public boolean isAfter(MyDateTime anotherDateTime) {
        return !isBefore(anotherDateTime);
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        int count;
        int anotherDateTimeMinute = anotherDateTime.turnToMinute();
        int dateTimeMinute = turnToMinute();
        count = Math.abs(anotherDateTimeMinute - dateTimeMinute);
        if (count < 60 * 24) {
            if (count % 60 == 0) {
                count = count / 60;
                return String.valueOf(count) + " hour(s)";
            } else {
                return String.valueOf(count / 60) + " hour(s) " + String.valueOf(count % 60) + " minute(s)";
            }
        } else {
            if (count % (60 * 24) == 0)
                return String.valueOf(count / (60 * 24)) + " day(s) ";
            int dayDiff = count / (60 * 24);
            count = count - (dayDiff * 60 * 24);
            int hourDiff = count / 60;
            count = count - (hourDiff * 60);
            int minuteDiff = count;
            if (hourDiff != 0 && minuteDiff != 0)
                return String.valueOf(dayDiff) + " day(s) " + String.valueOf(hourDiff) + " hour(s) " + String.valueOf(minuteDiff) + " minute(s) ";
            else if(hourDiff == 0 && minuteDiff != 0)
                return String.valueOf(dayDiff) + " day(s) " + String.valueOf(minuteDiff) + " minute(s) ";
            else
                return String.valueOf(dayDiff) + " day(s) " + String.valueOf(hourDiff) + " hour(s) ";

        }

    }
    public int turnToMinute(){
        int yearMinute = date.year * 365 * 24 * 60 ;
        int forMonthMinute = 0;
        for(int i = 0 ; i < date.month ; i++){
            forMonthMinute += MyDate.maxDays[i];
        }
        int monthMinute = forMonthMinute * 24 * 60;
        int dayMinute = date.day * 24 * 60;
        int hourMinute = time.hour * 60;
      return yearMinute + monthMinute + dayMinute + hourMinute + time.minute ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyDateTime myDate = (MyDateTime) o;
        return date.day == myDate.date.day && date.month == myDate.date.month && date.year == myDate.date.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(date.day, date.month, date.year);
    }


}
