package shapes2d;

public class Square {
    private double side;

    public Square(double side) {
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public double area(){
        return this.side * this.side;
     }

    @Override
    public String toString() {
        return "Square's side: " + this.side+
                " Area: " + area() ;

    }
}
