package shapes2d;

public class Circle {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double area(){
        return Math.PI* this.radius * this.radius;
    }
    public double perimeter(){
        return 2 * Math.PI * this.radius;
    }

    @Override
    public String toString() {
        return "Circle's radius: " +this.radius+
                " Area: " + area() ;
    }
}
