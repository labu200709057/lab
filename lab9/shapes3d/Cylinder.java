package shapes3d;
import shapes2d.Circle;

public class Cylinder extends Circle {
    private double height;
    public Cylinder(double radius,double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getRadius() {
        return super.getRadius();
    }

    @Override
    public void setRadius(double radius) {
        super.setRadius(radius);
    }

    @Override
    public double area() {
        return 2 * super.area() + (super.perimeter() * this.height) ;
    }
    public double volume(){
        return super.area() * this.height;
    }

    @Override
    public String toString() {
        return "Cylinder's height: " + this.height +
                " Radius: " + super.getRadius() + " Area: "+
                this.area() + " Volume: "+this.volume();
    }
}
