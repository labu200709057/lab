package shapes3d;
import shapes2d.Square;
public class Cube extends Square {
    public Cube(double side) {
        super(side);
    }

    @Override
    public double getSide() {
        return super.getSide();
    }

    @Override
    public void setSide(double side) {
        super.setSide(side);
    }

    @Override
    public double area() {
        return 6* super.area();
    }
    public double volume(){
        return super.area() * super.getSide();
    }

    @Override
    public String toString() {
        return "Cube's side: " + super.getSide() + " Area: "+ this.area()+
                " Volume: " + this.volume();
    }
}
