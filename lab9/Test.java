import shapes2d.Circle;
import shapes2d.Square;
import shapes3d.Cylinder;
import shapes3d.Cube;
public class Test {
    public static void main(String[] args){
        Circle circle = new Circle(3);
        Square square = new Square(5);
        Cylinder cylinder = new Cylinder(6,10);
        Cube cube = new Cube(20);
        System.out.println(circle.toString());
        System.out.println(square.toString());
        System.out.println(cylinder.toString());
        System.out.println(cube.toString());

    }
}
