import java.util.Objects;

public class MyDate {
    int day;
    int month;
    int year;
    int[] thirtyday = {4,6,9,11};
    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public boolean isLeapYear(){
        if(year % 4 == 0){
            if(year % 100 == 0){
                if(year % 400 == 0){
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }


    public boolean isThirtyDay(){

        for(int formonth : thirtyday){
            if(month == formonth)
                return true;
        }
        return false;
    }

    public void incrementDay(){
        if((isLeapYear()) && (month == 2) && (day == 29)){
            month++;
            day = 1;
        }
        else if (!isLeapYear()&& month== 2 && day == 28){
            month++;
            day = 1;
        }
        else if(day == 30 && isThirtyDay()){
            day = 1;
            month++;
        }
        else if (day == 31){
            day = 1;
            if(month ==12){
                year++;
                month = 1;
            }
            else
                month++;
        }

        else
            day++;
    }

    public void incrementDay(int valueOfIncrement){
        for(int i = 0 ; i < valueOfIncrement;i++){
            incrementDay();
        }
    }

    public void decrementDay(){
        if(isLeapYear() && month == 3){
            month--;
            day = 29;
        }
        else if(!isLeapYear() && month ==3){
            month--;
            day = 28;
        }
        else if(day==1){
            if(month == 1){
                year--;
                month = 12;
                day = 31;
            }
            else{
                month = month - 1;
                if(isThirtyDay()){
                    day = 30;
                }
                else
                    day = 31;
            }
        }
        else
            day--;
    }

    public void decrementDay(int valueOfDecrement){
        for(int i = 0;i < valueOfDecrement;i++){
            decrementDay();
        }

    }

    public void incrementMonth(){
        if(isLeapYear() && month == 1 && day >= 28){
            month = 2;
            day = 29;
        }
        else if(!isLeapYear() && month == 1 && day >=28){
            month = 2;
            day = 28;
        }
        else if(month== 12){
            year++;
            month = 1;
        }
        else if (month != 7 && !isThirtyDay() ){
            if(day > 30){
                month++;
                day = 30;
            }
            else
                month++;
        }

        else{
            month++;
        }

    }

    public void incrementMonth(int valueOfIncrement){
        for(int i = 0;i < valueOfIncrement;i++){
            incrementMonth();
        }

    }

    public void decrementMonth(){
        if(isLeapYear() && month == 3 && day >= 28){
            month = 2;
            day = 29;
        }
        else if(!isLeapYear() && month == 3 && day >= 28){
            month = 2;
            day = 28;
        }
        else if(month == 1){
            year--;
            month = 12;
        }
        else if(month == 2 || month == 8){
            month--;
        }
        else if(!isThirtyDay() && day >=30){
            day = 30;
            month--;
        }


    }

    public void decrementMonth(int valueOfDecrement){
        for(int i = 0 ; i < valueOfDecrement;i++){
            decrementMonth();
        }
    }

    public void incrementYear(){
        if(isLeapYear()&& month==2 && day==29){
            day=28;
        }
        year++;

    }

    public void incrementYear(int valueOfIncrement){
        if (month==2&&day==29) {
            if (valueOfIncrement % 4 != 0) {
                day = 28;
            }
            for(int j = 0 ; j < valueOfIncrement;j++){
                year++;
            }
        }
        else {
            for(int i= 0;valueOfIncrement>i;i++)
                incrementYear();
        }

    }

    public void decrementYear(){
        if(isLeapYear() && month ==2 && day==29){
            day=28;
        }
        year--;

    }

    public void decrementYear(int valueOfDecrement){
        if (month==2&&day==29) {
            if (valueOfDecrement % 4 != 0) {
                day = 28;
            }
            for(int j = 0 ; j < valueOfDecrement;j++){
                year--;
            }
        }
        else {
            for(int i= 0;valueOfDecrement>i;i++)
               decrementYear();
        }
    }

    public boolean isBefore(MyDate date){
        int olddate = Integer.parseInt(toString().replace("-",""));
        int newdate = Integer.parseInt(date.toString().replace("-",""));
        return olddate < newdate;
    }

    public boolean isAfter(MyDate date){
        return !isBefore(date);
    }

    public int dayDifference(MyDate date){
        int count = 0;
        if(isBefore(date)){

            while(!date.equals(this)){
                date.decrementDay();
                count++;
            }
        }
        else{
            while(!date.equals(this)){
                date.incrementDay();
                count++;
            }
        }
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyDate myDate = (MyDate) o;
        return day == myDate.day && month == myDate.month && year == myDate.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, month, year);
    }

    @Override
    public String toString() {
        String dayString = String.valueOf(day) ;
        String monthString = String.valueOf(month) ;
        if(day<10){
            dayString =  "0"+ dayString;
        }
        if(month<10){
            monthString = "0"+ monthString;
        }
        return year + "-" + monthString + "-" + dayString;
    }

}
