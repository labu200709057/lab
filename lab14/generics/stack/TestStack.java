package generics.stack;

public class TestStack {
    public static void main(String[] args){
       testStack(new StackImpl<Number>());

    }
    public static void testStack(Stack<Number> stack){
        stack.push(5);
        stack.push(6);
        stack.push(2);
        stack.push(11.3);
        stack.push(2333333333L);
        System.out.println("stack1" + stack.toList());
        Stack<Integer> stack2 = new StackImpl<>();
        stack2.push(22);
        stack2.push(44);
        stack2.push(11);
        System.out.println("stack2" + stack2.toList());

        stack.addAll(stack2);
        System.out.println("stack1" + stack.toList());
        while(!stack.empty()){
            System.out.println(stack.pop());
        }
        System.out.println(stack.toList());
    }
}
