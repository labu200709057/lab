public class GCDRec {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Please provide two integers");
        }
        int number1 = Integer.parseInt(args[0]);
        int number2 = Integer.parseInt(args[1]);

        int result = gcd(Math.max(number1, number2), Math.min(number1, number2));

        System.out.println("GCD of " + number1 + " and " + number2 + " = " + result);
    }

    public static int gcd(int number1, int number2) {
        int remainder = number1 % number2;
        if (remainder == 0)
            return number2;
        return gcd(number2, remainder);


    }
}