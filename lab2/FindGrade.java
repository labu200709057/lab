public class FindGrade {
    public static void main(String[] args){
        int score = Integer.parseInt(args[0]);
        if (score > 100 || score < 0){
            System.out.println("It is not a valid score!");
        }
        else if (90 <= score){
            System.out.println("Your grade is A");
        }
        else if (80 <= score){
            System.out.println("Your grade is B");
        }
        else if (70 <= score){
            System.out.println("Your grade is C");
        }
        else if (60 <= score){
            System.out.println("Your grade is D");
        }
        else {
            System.out.println("Your grade is F");
        }


    }
}
